var img_ele = null;

function zoom(zoomincrement) {
  img_ele = document.getElementById('drag-img');
  var pre_width = img_ele.getBoundingClientRect().width,
  		pre_height = img_ele.getBoundingClientRect().height;
  img_ele.style.width = (pre_width * zoomincrement) + 'px';
  img_ele.style.height = (pre_height * zoomincrement) + 'px';
  img_ele = null;

  imageMapResize();

}

$(function(){
document.getElementById('zoom-out').addEventListener('click', function() {
  zoom(0.5);
});
document.getElementById('zoom-in').addEventListener('click', function() {
  zoom(1.5);
  adjustImage();
});
});
function adjustImage(){
  var leftPosition = $("#drag-img").css("left");
  $("#drag-img").css("left", (leftPosition - 500) + "px");
}

function start_drag() {
  img_ele = this;
  x_img_ele = window.event.clientX - document.getElementById('drag-img').offsetLeft;
  y_img_ele = window.event.clientY - document.getElementById('drag-img').offsetTop;
	console.log("start drag");
}

function stop_drag() {
  img_ele = null;
}

function while_drag() {
  var x_cursor = window.event.clientX;
  var y_cursor = window.event.clientY;
  if (img_ele !== null) {
    img_ele.style.left = (x_cursor - x_img_ele) + 'px';
    img_ele.style.top = ( window.event.clientY - y_img_ele) + 'px';
      //console.log('dragging > img_left:' + img_ele.style.left+' | img_top: '+img_ele.style.top);
  }
}
$(function(){
document.getElementById('drag-img').addEventListener('mousedown', start_drag);
document.getElementById('container').addEventListener('mousemove', while_drag);
document.getElementById('container').addEventListener('mouseup', stop_drag);
});
