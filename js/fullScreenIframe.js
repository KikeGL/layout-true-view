$(function(){
  $('#fullScreenButton').on("click",fullscreen);
    buttonText  = $("#fullScreenButton").text();
});

  // detect enter or exit fullscreen mode
  document.addEventListener('webkitfullscreenchange', fullscreenChange);
  document.addEventListener('mozfullscreenchange', fullscreenChange);
  document.addEventListener('fullscreenchange', fullscreenChange);
  document.addEventListener('MSFullscreenChange', fullscreenChange);

  function fullscreen() {
    // check if fullscreen mode is available
      var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
          (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
          (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
          (document.msFullscreenElement && document.msFullscreenElement !== null);
      if(!isInFullScreen){
          if (document.fullscreenEnabled ||
              document.webkitFullscreenEnabled ||
              document.mozFullScreenEnabled ||
              document.msFullscreenEnabled) {
              enterFullScreen();

          }
          else {
              document.querySelector('.error').innerHTML = 'Your browser is not supported';
          }
      }else{
          exitFullScreen();
      }

  }
  function enterFullScreen() {
      // which element will be fullscreen
      var iframe = document.querySelector('#iframeWrapper');
      // Do fullscreen
      if (iframe.requestFullscreen) {
          iframe.requestFullscreen();
          $("#fullScreenButton").text("Salir");
      } else if (iframe.msRequestFullscreen) {
          iframe.msRequestFullscreen();
          $("#fullScreenButton").text("Salir");
      }
  }


  function exitFullScreen() {
      if (document.msExitFullscreen) {
          document.msExitFullscreen();
          $("#fullScreenButton").text(buttonText);
      }
  }

  function fullscreenChange() {
    if (document.fullscreenEnabled ||
         document.webkitIsFullScreen || 
         document.mozFullScreen ||
         document.msFullscreenElement) {

    }
    else {

      //console.log('exit fullscreen');
        //var iframe = document.querySelector('iframe');
        //iframe.src = iframe.src;
    }
    // force to reload iframe once dosto prevent the iframe source didn't care about trying to resize the window
    // comment this line and you will see
  }