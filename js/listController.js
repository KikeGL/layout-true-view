//first level data
//li id
var id ="1N";
//div class
var name ="1N";
//a text
var levelName = "Primer nivel";


$(function(){
		$(".level > div").click(function () {
		id = $(this).closest('li').attr('id');
		name = $(this).attr('class');
		$("#iframe-trueview").attr('src',"./"+name+"/SiteMap.htm");
		levelName = $("#"+id+" > div > a").text();
	});
	
	$('#iframe-trueview').on("load", function () {
		$('.selected').removeClass('selected');
		$("#" + id).addClass("selected");
		$("#title").text(levelName);
	});
});