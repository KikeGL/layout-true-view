# Trueview visualizer

SPA to visualize truviews

## Getting Started
Download this project and uncompress it or use the cmd (git clone https://gitlab.com/KikeGL/layout-true-view.git)
Paste all the truview folders (the ones containing the sitemap.htm) in the root
folder of the project. It should look like this:
![img](https://i.imgur.com/TXGOWNZ.png)

Edit the "platformInfo.js" file and change the variables so they match the info
from the platform. For this example:
![img](https://i.imgur.com/4px7dE5.jpg)

Note: This was made to work with Leica TruView Local (a free viewer for Internet Explorer to access TruView files),
for <a href="www.iktansoft.com">Iktansoft</a>

## Built With

* [Angularjs](https://angularjs.org) - The javascript framework used
* [JQuery](https://jquery.com) - Used to manipulate the DOM

## Authors

* **Jorge Sala** - *web development* - (https://gitlab.com/KikeGL)
* **Rodolfo Ruvalcaba** - *web design* -
