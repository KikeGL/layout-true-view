var app = angular.module('myApp', []);

//Data to modify
//name = nombre que se va a mostrar folder= nombre de la carpeta
var data = [{name:'Primer nivel',folder:'1N'},
                {name:"Subnivel", folder: "subnivel"},
                {name: "Zona de mareas", folder:"zona de mareas"}];


app.filter('removeSpaces', [function() {
    return function(string) {
        if (!angular.isString(string)) {
            return string;
        }
        return string.replace(/[\s]/g, '');
    };
}]);

app.controller('myCtrl', function($scope) {
    $scope.levels = data;
    angular.forEach($scope.levels, function(value) {
        value.folder = encodeURI(value.folder);
        console.log(value.folder);  
    });
});

